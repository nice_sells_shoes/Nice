# 零、项目开发要求
> 必须使用git进行团队协作

# 一、Git基本使用
> 分布式的版本管理系统

> 集中式的版本管理  SVN

1. 安装
> [下载](https://git-scm.com/download/win)

> 在线远程仓库
[gitHub](https://github.com/)
[码云](https://gitee.com/)


2. 对git进行最小配置  【重要】

+ 配置命令
```
配置用户名
git config --global user.name 用户名

配置用户邮箱
git config --global user.email 用户邮箱
```

+ 查看最小配置
```
git config --global --list
```

3. 创建仓库及基本操作  【重要】
    + git init       在本地初始化git仓库
    + 添加到暂存区     
       - git add 文件名称      将某个指定文件，添加到暂存区
       - git add .            将所有被改动过的文件，都纳入暂存区
    + git status  查看版本状态
    + git commit -m'版本描述文字'

4. 版本回退

    + git log   
    + git reset --hard HEAD^    回退至上一次commit
    + git reset --hard dcbc3b    回退至指定的某个commit

> 头指针(HEAD->)  指向当前版本信息位置


5. 分支概念及使用 【重要】

master主分支--vue基本骨架-----------------------购物车--------master分支---完整项目
                          |                 |           |
                          |---Trump---购物车|           |
                          |                            |
                          |---Mask---个人中心----------|




 + git checkout -b 分支名称     新建分支并切换到新分支
 + git checkout 分支名称        切换到指定分支
 + git log      【重要】一定要在写代码前，确定自己是处于自己的子分支，不要在master分支直接编写功能代码   
 + git merge 子分支名称   【强调】先checkout到主分支
    - git checkout master  
    - git merge Mask  


6. 任务

 + git安装
 + git基本配置
 + git的基本使用流程
 + git分支的概念及其使用方式
 

7. 本地仓库与远程仓库合并过程  【比较繁琐，不推荐】
> 在本地 在远程分别新建了两个仓库
    + 创建远程仓库
    + 在本地仓库目录下连接远程仓库
    ```
    git remote add 仓库名称(默认是origin) 远程仓库地址 
    ```
    + 获取远程仓库的分支信息
    ```
    git fetch origin
    ```
    + 将远程仓库的master分支，合并到本地的master分支上
    ```
    git merge --allow-unrelated-histories origin/master
    ```

    + 向远程仓库提交本地master分支
    ```
    git push --set-upstream origin master
    ```

    + 新增功能后，重新push
    ```
    git push 
    ```

7-1. 在Linux中输入并保存信息
    + 英文输入状态下
    + 按 i  进入编辑状态
    + 输入git操作的动作描述
    + 按 ESC  退出编辑状态
    + :wq  保存退出

8. 先新建远程仓库，然后在本地克隆 【推荐采用这种方式】

+ 在码云新建远程仓库

+ 在本地目录下，克隆远程仓库
```
git clone 仓库地址
```

+ 命令终端指定到仓库目录下

+ 拆一个自己的子分支

+ 在子分支上进行开发

+ 将代码提交到远程仓库
```
git push
```


9. 提交代码与拉取代码

> 第一次pull下来的master分支代码是一样的
> A成员在本地master分支进行了开发，并push
> B成员也在本地master分支进行开发，并push
> 产生冲突
> 如何避免冲突，不要同时操作同一个分支


# 二、团队配合

1. 【组长负责】新建组织 

2. 【组长负责】在组织中的【设置】-【成员管理】下，邀请小组成员

3. 【组长负责】在组织下创建仓库

4. 【组长负责】克隆空仓库到本地，并在该仓库中创建vue项目基本结构，并git push到远程仓库
    + 【注意】把项目所需要的的所有的公共文件都要配置好

5. 所有小组成员，克隆vue项目基本结构到本地

6. 所有小组成员，拆分自己的子分支【强调】

7. 所有小组成员在自己的分支上完成开发任务

8. 所有小组成员，向远程仓库提交自己的分支
```
git push origin 分支名

git branch 查看分支列表
```

9. 【组长牵头】将所有的子分支拉取到本地
```
git pull   拉取远程仓库的所有内容
git branch --all    查看本地所有分支信息
```

10. 【组长牵头】依次将子分支代码合并到主分支，并解决冲突 【至少每一天合并依次】
```
git checkout master
git merge 子分支名称
```

11. 所有组员，拉取最新的master分支代码，并合并到自己本地子分支上

```
git pull
git checkout 自己分支
git merge master    将最新主分支代码合并到自己的子分支
```

12. 继续新的任务开发，直到项目完成
> 7步~11步之间，持续执行




