import request from '../utils/request'

//首页商品列表渲染
export const getGoodsList = () => {
    return request.get('/userList?page=0&size=3')
}
export const getGoodsList2 = () => {
    return request.get('/order/appraiser?page=0&size=10')
}
export const getGoodsList3 = () => {
    return request.get('/rangeDetail?rangeId=19')
}


//根据商品id渲染头部
export const getGoodById = () => {
    // return request.get(`/userDetail?appraiserId=${id}`)
    return request.get(`/userList?page=0&size=3`)
}
//根据商品id渲染底部
export const getGoodById2 = (id) => {
    // return request.get(`/userDetail?appraiserId=${id}`)
    return request.get(`/order/appraiser?appraiserId=${id}&page=0`)
}


//根据商品call渲染头部
export const getGoodcall = (id) => {
    // return request.get(`/userDetail?appraiserId=${id}`)
    return request.get(`/order/appraiser?page=${id}&size=10`)
}
//根据商品call渲染底部
export const getGoodcall2 = (appraiserId) => {
    // return request.get(`/userDetail?appraiserId=${id}`)
    return request.get(`/userDetail?appraiserId=${appraiserId}`)
}
