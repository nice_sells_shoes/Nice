import request from '../utils/request2'

export const userReg = (data)=>{
    return request.post('/user/reg',data)
}

export const userLogin = (data)=>{
    return request.post('/user/login',data)
}



export const yanzhen =(data)=>{
    let uInfo = localStorage.getItem('userInfo')
    if(uInfo){
        var {token} = JSON.parse(uInfo)
    }else{
         token = ''
    }
    return request.get(`/yanzhen?token=${token}`,data)
}
// 提交洗护订单
export const washorder =(data)=>{
    return request.post('/user/washorder',data)
}

export const getWashorder = (id)=>{
    // console.log(id);
    return request.post('/user/getWashorder',id)
}

export const recovery = (data)=>{
    return request.post('/user/recovery',data)
}

export const getRecovery = (id)=>{
    return request.post('/user/getRecory',id)
}