import Vue from "vue";
import VueRouter from "vue-router";
import layout from "../views/layout.vue"
import IndexCard from '../views/index/IndexCard.vue'
import ShoesClass from '../views/classfi/ShoesClass.vue'
import appraisal from '../views/jiandin/appraisal.vue'
import WashProtect from '../views/xihu/WashProtect.vue'
import Freei from '../views/jiandin/Freei.vue'
import Dolaundary from '../views/xihu/Dolaundary.vue'
import Address from '../components/Address.vue'
import AddressManage from '../components/AddressManage.vue'
import Coupon from '../components/Coupon.vue'
import more from "../views/moreShoes/MoreShoes.vue"
import Must from "../views/jiandin/Must.vue"
import Personal from "../views/jiandin/Personal.vue"
import GuijiaIndex from "../views/gujia/GujiaIndex.vue"
import perso from "../views/jiandin/perso.vue"
import userIndex from "../views/user/userIndex.vue"
import GuiJiaRes from "../views/gujia/GuiJiaRes.vue"
import login from "../components/login.vue"
import resiger from "../components/resiger.vue"
import huishouDingdan from "../views/user/huishouDingdan.vue"
import WashDetail from "../views/xihu/WashDetail.vue"
import Mobilepay from "../views/xihu/Mobilepay.vue"
import WashOrder from "../views/xihu/WashOrder.vue"
import HuishouInfo from "../views/user/HuishouInfo.vue"


import {yanzhen} from '../api/userApi'

Vue.use(VueRouter);         

const routes = [
    {
      path:'/',
      component:layout,
      children:[
        {
          path:'/',
          meta:{
            path:'/'
          },
          component:IndexCard
        },
        {
          path:'appraisal',
          component:appraisal,
          

        },
        {
          path:'WashProtect',
          component:WashProtect

        },
        {
          path:'userIndex',
          component:userIndex,
        }
       
      ]
    },
    {
      path:'/huishouDingdan',
      component:huishouDingdan
    },
    {
      path:'/login',
      component:login
    },
    {
      path:'/resiger',
      component:resiger
    },
    {
      path:'/GuijiaIndex/:id',
      component:GuijiaIndex
    },
    {
      path:'/more/:key',
      component:more
    },
      {
      path:'/Appraisal',
      component:appraisal,
    },
    {
      path:'/freei',
      component:Freei,
    },{
      path:'/must',
      component:Must,
    },
    {
      path:'/personal/:id',
      component:Personal,
    },
    {
      path:'/Perso/:id',
      component:perso,
    },
    {
      path:'/shoeClass',
      component:ShoesClass
    },
    {
      path:'/Freei',
      component: Freei
    },
    {
      path:'/Dolaundary',
      component:Dolaundary
    },
    {
      path:'/Address',
      component:Address
    },
    {
      path:'/AddressManage/:id',
      component:AddressManage
    },
    {
      path:'/Coupon',
      component:Coupon
    },
    {
      path:'/gjRes',
      component:GuiJiaRes

    },
    {
      path:'/WashDetail',
      component:WashDetail
    },
    {
      path:'/Mobilepay',
      component:Mobilepay
    },
    {
      path:'/WashOrder',
      component:WashOrder
    },
    {
      path:'/HuishouInfo/:id',
      component:HuishouInfo
    }
];



const router = new VueRouter({
  // mode: "history",
  // base: process.env.BASE_URL,
  routes
});


router.beforeEach(async (to,from,next)=>{
 
  if(to.path=='/Freei' || to.path=='/gjRes' || to.path=='/Dolaundary'|| to.path=='/userIndex'){
    let res = await yanzhen()
    if(res.data.code ==1){
      next()
    }else{
      next('/login')
    }
    
  }
    next()
    
})
export default router;
