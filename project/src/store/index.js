import Vue from "vue";
import Vuex from "vuex";
import cart from "./cart"
import user from "./user"
import shoes from "./shoesInfo"
Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    cart,
    user,
    shoes
  }
});
