export default {
    state: () => {
        return {
            allinfo: null,

        }
    },
    mutations: {
        shoeInfoAdd(state,newInfo){
            state.allinfo = newInfo 
            console.log(state.allinfo)
        }
    },
    actions:{
        shoeInfoAdd ({commit},data){
            commit('shoeInfoAdd',data)
        }
    }
}