export default{
    state:()=>({
        addressInfo:[
            {
                // id:0,
                name:"陈宇",
                tel:'13988883245',
                province:"山西省",
                city:"太原市",	
                county:"古交市",              
                addressDetail:"平安镇",
                isDefault:true	
            },
        ],
        checkedAds:
            {
                // id:0,
                name:"陈宇",
                tel:'13988883245',
                province:"山西省",
                city:"太原市",	
                county:"古交市",              
                addressDetail:"平安镇",
                isDefault:true	
            },
        
    }),
    mutations: {
        adsInfoMut(state, {newAds,idx}) {
            if(idx==-1){
                // 新增
                state.addressInfo.push(newAds);
                // console.log(state.addressInfo);
            }else{
                // 修改
                state.addressInfo[idx]=newAds;
                // console.log(newAds);
            }
           
        },
        // 删除
        delAds(state,idx){
            let {addressInfo}=state;
            addressInfo.splice(idx,1);
        },
        commitAds(state,idx){
            let i=idx;
            // console.log(state.addressInfo[i]);
            state.checkedAds=state.addressInfo[i];
            console.log(state.checkedAds);
        }
    }
}