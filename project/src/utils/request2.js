import axios from 'axios'
import {Notify} from 'vant'
var instanceLogadREG = axios.create({
    baseURL:'http://localhost:3000'
})

instanceLogadREG.interceptors.request.use(function (config) {
// 在发送请求之前做些什么

    return config;
  }, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  });

// 添加响应拦截器
instanceLogadREG.interceptors.response.use(function (response) {
  // console.log(response)
  let msg = response.data.msg
  let type = response.data.code?'success':'danger'
  Notify({ type: type, message: msg })
    
    return response;
  }, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
  });

  
export default instanceLogadREG