var mongoose  = require('mongoose')

var userSchema = new mongoose.Schema({
    username:String,
    password:String,
    tel:String,
    sms:String,
})

var dingdanSchema = new mongoose.Schema({
    userId:String,
    recoveryAllData:Object,
})

var xihuSchems= new mongoose.Schema({
    userId:String,
    washAllData:Object,
})

var userModel = mongoose.model('userModel',userSchema)

var dingdanModel = mongoose.model('dingdanModel',dingdanSchema)

var xihuModel=mongoose.model('xihuModel',xihuSchems)

module.exports = {userModel,dingdanModel,xihuModel}
