var express = require('express');
var router = express.Router();
var {userModel} = require('../module/userModule')
var {dingdanModel} = require('../module/userModule')
var {xihuModel} =require("../module/userModule")
var bcrypt = require('bcryptjs')
var salt = bcrypt.genSaltSync(10);
var jwt =require('jsonwebtoken');
const { request } = require('express');
var secret = 'yjjhahahaaa'



/* GET users listing. */
router.post('/reg', function(req, res, next) {
  console.log(req.body)
  let {username,password,tel,sms} = req.body;

  userModel.find({username}).then((result)=>{
    
    if(result[0]){
      res.send({
        code:0,
        msg:'对不起，名字太受欢迎了，换一个吧'
      })
      return
    }

    let hash = bcrypt.hashSync(password,salt)
    new userModel({
      username,
      password:hash,
      tel,
      sms
    }).save().then(()=>{
      res.send({
        code:1,
        msg:'恭喜你注册成功，可以愉快地交易了！'
      })
    })


  })
  
});

router.post('/login',function(req,res,next){
    let {username,password} = req.body
    console.log(username,password)
    userModel.findOne({username}).then((result)=>{
      if(!result){
        res.send({
          code:0,
          msg:'你还没有注册哦！请前往注册'
        })
        return
      }

      let hash = result.password
      let bool = bcrypt.compareSync(password,hash)
      let data = null
      if(bool){
        let token = jwt.sign({
          time:new Date().getTime(),
          limit:1000*60*60*24*7
        },secret)
        result.password=''
        data = {
          ...result,
          token
        }
        res.send({
          code:1,
          msg:'恭喜你！登录成功了!',
          data
        })
      }else{
        res.send({
          code:0,
          msg:'你密码错了，兄嘚！',
          data
          
        })
      }
    })

})
// 提交洗护订单
router.post('/washorder',function(req,res,next){
  console.log(req.body);
  let washAllData=req.body;
  let userId=req.body.userId;
  new xihuModel({
    userId,
    washAllData
  }).save().then(()=>{
    res.send({
      code:1,
      msg:"已经提交了心仪的订单哦！"
    })
  })
})
// 渲染所有洗护订单
router.post('/getWashorder',function(req,res,next){
  // console.log(req.body);
  let userId =req.body.userId;
  xihuModel.find({"userId":`${userId}`}).then((result)=>{
    console.log(result);
    // res.send({
    //   code:1,
    //   msg:"成功",
    //   data:[...result]

    // })
    if(result[0]){
      res.send({
        code:1,
        msg:'你有很多订单哦！',
        data:result
      })
      return
    }
    res.send({
      code:0,
        msg:'你还没有订单哦！快去康康吧',
        data:undefined
    })
    
  })
})

router.post('/recovery',function(req,res,next){
  let recoveryAllData = req.body
  let userId = req.body.userId
  // console.log(recoveryAllData)
  new dingdanModel({
    userId,
    recoveryAllData
  }).save().then(()=>{
    res.send({
      code:1,
      msg:'已经加入了回收订单了哦！'
    })
  })
//  console.log(recoveryInfo)
})

router.post('/getRecory',function(req,res,next){
  let userId = req.body.userId

  dingdanModel.find({"userId":`${userId}`}).then((result)=>{
    if(result[0]){
      res.send({
        code:1,
        msg:'你有很多订单哦！',
        data:result
      })
      return
    }
    res.send({
      code:0,
        msg:'你还没有订单哦！快去康康吧',
        data:undefined
    })

  })
})
module.exports = router;
